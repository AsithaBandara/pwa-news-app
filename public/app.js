const apiKey = '4bd7c13857334f239f3855c1a0419860'
const main = document.querySelector('main')
const sourceSelector = document.querySelector('#sourceSelector')

window.addEventListener('load', e => {
    updateNews();

    if ('serviceWorker' in navigator) {
      try {
        navigator.serviceWorker.register('sw.js');
        console.log("SW registered !");
      } catch (e) {
        console.log("SW registration failed !");
      }
    }
});

async function updateNews() {
    const res_news = await fetch(`https://newsapi.org/v2/top-headlines?sources=TechCrunch&apiKey=${apiKey}`);
    const json_news = await res_news.json();

    main.innerHTML = json_news.articles.map(createArticle).join('\n');
}

function createArticle(article) {
    return `
        <div class="row article">
            <div class="col-sm-12">
                <a href="${article.url}">
                <h2>${article.title}</h2>
                <img class="img-responsive" src="${article.urlToImage}">
                <p>${article.description}</p>
                </a>
            </div>
        </div>
    `;
}
